import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

export interface Message {
  id: string;
  sender: string;
  reciever: string;
  text: string;
  date?: string;
}

@Injectable({
  providedIn: 'root',
})
export class MockMessageApiService {
  constructor() {}

  getMessages(): Observable<Message[]> {
    return of([
      { id: '32', sender: '2', reciever: '89748923', text: 'message text' },
      {
        id: '99345',
        sender: '4324',
        reciever: '25345345',
        text: 'message text2',
      },
    ]);
  }
}
