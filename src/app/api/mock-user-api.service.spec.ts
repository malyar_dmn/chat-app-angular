import { TestBed } from '@angular/core/testing';

import { MockUserApiService } from './mock-user-api.service';

describe('MockUserApiService', () => {
  let service: MockUserApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MockUserApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
