import { TestBed } from '@angular/core/testing';

import { MockMessageApiService } from './mock-message-api.service';

describe('MockMessageApiService', () => {
  let service: MockMessageApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MockMessageApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
