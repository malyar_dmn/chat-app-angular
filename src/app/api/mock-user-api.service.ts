import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { User } from '../services/user.service';

@Injectable({
  providedIn: 'root',
})
export class MockUserApiService {
  constructor() {}

  getUsers(): Observable<User[]> {
    return of([
      { id: '1', username: 'Max' },
      { id: '2', username: 'Dmn' },
      { id: '3', username: 'Gachi' },
      { id: '4', username: 'test' },
    ]);
  }
}
