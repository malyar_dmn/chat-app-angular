import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, map, Observable, tap } from 'rxjs';
import { MockUserApiService } from '../api/mock-user-api.service';

export interface User {
  id: string;
  username: string;
}

@Injectable({
  providedIn: 'root',
})
export class UserService {
  myUserId: string = 'my_id';
  users$: Observable<User[]>;
  constructor(private mockUserApiService: MockUserApiService) {
    this.users$ = this.mockUserApiService.getUsers();
  }

  getUsers(): Observable<User[]> {
    return this.mockUserApiService.getUsers();
  }

  getUsersAndStore(): void {
    // this.getUsers().pipe(tap(users => 'this.store.dispatch'))
  }
}
