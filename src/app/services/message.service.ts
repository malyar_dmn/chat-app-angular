import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  Message,
  MockMessageApiService,
} from '../api/mock-message-api.service';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  constructor(private mockMessageApiService: MockMessageApiService) {}

  getMessages(): Observable<Message[]> {
    return this.mockMessageApiService.getMessages();
  }
}
