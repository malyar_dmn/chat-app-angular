import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User, UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  users$: Observable<User[]>;
  constructor(private userService: UserService) {}

  selectUserHandler(userId: string) {
    console.log('user id', userId);
  }

  ngOnInit(): void {
    this.users$ = this.userService.getUsers();
  }
}
