import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  constructor() {}

  @Input() user: User;
  @Output() onSelectUser = new EventEmitter<string>();

  selectUserHandler(userId: string) {
    this.onSelectUser.emit(userId);
  }

  ngOnInit(): void {}
}
